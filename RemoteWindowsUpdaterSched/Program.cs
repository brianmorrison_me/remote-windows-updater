﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RemoteWindowsUpdaterLib;

namespace RemoteWindowsUpdaterSched
{
    class Program
    {
        //Call this program from powershell, passing in a date & time, using Invoke-Command
        static void Main(string[] args)
        {
#if DEBUG
            //Echo out arguments, useful in diagnostics
            foreach (var arg in args) Console.WriteLine(arg);
#endif
            //Clean up arguments passed from Powershell
            List<String> listArguments = new List<String>();
            foreach (var arg in args)
            {
                if (arg != "-ArgumentList") listArguments.Add(arg);
            }
            //Date Example:                             1-15-2016
            //Time Example (Should be in 24h format):   14:15
            if (listArguments[0] == "help" || listArguments[0] == "?")
            {
                //Displays help text for this program.
                //Console.WriteLine("**HELP TEXT**");
                return;
            }
            if (listArguments.Count != 2)
            {
                Console.WriteLine("Must have two arguments, date and time respectively. Program will exit.");
            }
            else
            {
                try
                {
                    DateTime dt = DateTime.Parse(listArguments[0] + " " + listArguments[1]);
                    Console.WriteLine("Parsed date/time: " + dt.ToString());
                    RWULib.ScheduleUpdates(dt);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Program failed to get date/time from arguments passed. Full exception detail:");
                    Console.WriteLine(e.ToString());
                }
            }
        }
    }
}
