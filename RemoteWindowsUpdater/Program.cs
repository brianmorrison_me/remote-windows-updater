﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32.TaskScheduler;
using RemoteWindowsUpdaterLib;
using WUApiLib;

namespace RemoteWindowsUpdater
{
    class Program
    {            
        static string sSource = "RemoteWindowsUpdater";
        static string sLog = "Application";

        //-----------Application switches
        static bool _reboot = false;
        static bool _downloadUpdates = false;
        static bool _installUpdates = false;
        static bool _waitBeforeExiting = false;
        static bool _scheduleUpdates = false;
        static int _inMinutes;
        static DateTime _date;
        static DateTime _time;

        static void Main(string[] args)
        {

#if DEBUG
            _downloadUpdates = true;
            _installUpdates = true;
            _reboot = false;
            _waitBeforeExiting = true;
            _scheduleUpdates = true;
            _inMinutes = 5;
#endif
            //Default scheduled task parameters are 'download install'
            foreach (string a in args)
            {
                if (a == "schedule") _scheduleUpdates = true;
                if (a == "download") _downloadUpdates = true;
                if (a == "install") _installUpdates = true;
                if (a == "reboot") _reboot = true;
                if (a == "wait") _waitBeforeExiting = true;
                bool intResult = Int32.TryParse(a, out _inMinutes);
            }

            //Check first if the updates are being scheduled. Then proceed to normal parameters.
            if(_scheduleUpdates)
            {
                Console.WriteLine("Schedule parameter applied. All other arguments will be ignored.");
                if(_inMinutes == 0) _inMinutes = 5;
                RWULib.ScheduleUpdatesInMinutes(_inMinutes);
            }
            else
            {
                Console.WriteLine("Starting program with the following switches");
                Console.WriteLine("Download: " + _downloadUpdates.ToString());
                Console.WriteLine("Install: " + _installUpdates.ToString());
                Console.WriteLine("Reboot: " + _reboot.ToString());

                RWULib.Update(_downloadUpdates, _installUpdates, _reboot);
            }

            //Check to see if this process was run manually (specified via parameter)
            if (_waitBeforeExiting)
            {
                Console.WriteLine("Waiting. Press any key to exit...");
                Console.Read();
            }
        }

    }
}
