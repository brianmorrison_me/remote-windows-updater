﻿# --- Modify the executable path in the remote scriptblock to match the location on your machines ---
#The $date and $time variables are passed as parameters to the command line application that is used to schedule patching
function Schedule-UpdatesOnServers
{
	<#
	.SYNOPSIS
	Schedules Windows Updates on servers.
	.DESCRIPTION
	The Schedule-UpdatesOnServers function uses a text file with a list of servers, one per line, and sends a remote command to the computer using PSRemoting. The command executes RemoteWindowsUpdaterSched.exe which creates a scheduled task on the server to execute RemoteWindowsUpdater.exe with the 'download install reboot' parameters. The user will be required to enter a confirmation code before the scheduler will proceed.
	.PARAMETER ServersTextFile
	Mandatory. A text file containing a list of server hostnames, one per line.
	.PARAMETER Date
	Mandatory. A date specified in 'mm-dd-yyyy' format.
	.PARAMETER Time
	Mandatory. A time specified in 'hh:mm' 24 hour format.
	.EXAMPLE
	Schedule a list of servers to reboot on 12/30/2016 at 8PM Central Time.
	Schedule-UpdatesOnServers -ServersTextFile "C:\servers.txt" -Date "12-30-2016" -Time "20:00"
	.NOTES
	This command should be executed in an elevated PowerShell session.
	#>

	param(
		[Parameter(Mandatory=$true)]
		[string]$ServersTextFile,
		[Parameter(Mandatory=$true)]
		[string]$Date,
		[Parameter(Mandatory=$true)]
		[string]$Time
	)

	#Specify $Date in 'mm-dd-yyyy' format
	#$date = "1-18-2016"
	#Specify $Time in 'hh:mm' 24 hour format
	#$time = "14:10" 

	#Servers.txt file (contains list of servers)
	#$serverstextfile = "servers.txt"

	#Specify file name to parse into Powershell. This should contain the name for 1 server on each line.
	$servers = get-content $ServersTextFile 

	#Rename text file so it doesn't get processed again for whatever reason.
	Copy-Item -Path "$serverstextfile" -Destination "$serverstextfile-processed.txt"
	Remove-Item $serverstextfile

	#Generate random string for confirmation.
	$randomstring = Get-Random

	Write-Host "The following servers will be processed on $date at $time :"
	$servers
	$userinput = Read-Host "Enter the following code to confirm scheduling [$randomstring]:"

	if($userinput -eq $randomstring) 
	{
		write-host "Proceeding..."
    
		#Create variable to hold the arguments that are passed into the scriptblock
		$args = @($Date,$Time)

		foreach ($s in $servers)
		{
			Write-Host "Scheduling patches for $s"

			#Create remote powershell session for the server $s
			$session = new-pssession -ComputerName $s

			#Execute the RemoteWindowsUpdaterSched.exe program to create a scheduled task to patch/reboot the server.
			$sbSchedulePatching = {
				param($pDate,$pTime)
				C:\RemoteWindowsUpdaterSched.exe -ArgumentList @($pDate,$pTime)
			}

			#Pass the above command to the server $s
			Invoke-Command -Session $session -ArgumentList @($Date,$Time) -ScriptBlock $sbSchedulePatching  

			#Remove the session to clear the variable for the next server
			Remove-PSSession -Session $session
		}
	}
	else
	{
		write-host "Code does not match. Please run the script again with the correct confirmation code."
	}
}