﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32.TaskScheduler;
using WUApiLib;

namespace RemoteWindowsUpdaterLib
{
    public class RWULib
    {
        // --- Path to the application, can use System.Reflection if necessary ---
        private static string pathToApp = "**YOUR PATH**";
        static UpdateCollection updatesToInstall = new UpdateCollection();
        static UpdateSession uSession = new UpdateSession();
        static IUpdateSearcher uSearcher;
        static ISearchResult sResult;

        public static void ScheduleUpdates(DateTime dt)
        {
            DateTime startTime = dt;
            if (DateTime.Now < dt)
            {
                try
                {
                    using (TaskService ts = new TaskService())
                    {
                        TaskDefinition td = ts.NewTask();
                        td.RegistrationInfo.Description = "Created with RemoteWindowsUpdater.exe. Schedules forced download & installation of patches, and forces reboot upon completion of process.";
                        TimeTrigger tt = new TimeTrigger();
                        tt.StartBoundary = startTime;
                        td.Triggers.Add(tt);
                        td.Actions.Add(new ExecAction(pathToApp, "download install reboot", null));
                        td.Principal.RunLevel = TaskRunLevel.Highest;
                        ts.RootFolder.RegisterTaskDefinition("RemoteWindowsUpdater", td, TaskCreation.CreateOrUpdate, "SYSTEM", null, TaskLogonType.ServiceAccount);
                    }
                    Console.WriteLine("Scheduled task created successfully. Patches will occur at " + startTime.ToString() + ".");
                }
                catch (Exception e)
                {
                    LogMessage("RemoteWindowsUpdater", "Unable to register scheduled task. Error: " + e.ToString());
                    Console.WriteLine(e.ToString());
                }
            }
            else
            {
                LogMessage("RemoteWindowsUpdater", "Error in scheduling task. Cannot schedule in the past...");
                Console.WriteLine("Error in scheduling task. inMinutes set to 0");
            }
        }

        public static void ScheduleUpdatesInMinutes(int inMinutes)
        {
            DateTime startTime = DateTime.Now + TimeSpan.FromMinutes(inMinutes);
            if (inMinutes != 0)
            {
                try
                {
                    using (TaskService ts = new TaskService())
                    {
                        TaskDefinition td = ts.NewTask();
                        td.RegistrationInfo.Description = "Created with RemoteWindowsUpdater.exe. Schedules forced download & installation of patches, and forces reboot upon completion of process.";
                        TimeTrigger tt = new TimeTrigger();
                        tt.StartBoundary = startTime;
                        td.Triggers.Add(tt);
                        td.Actions.Add(new ExecAction(pathToApp, "download install reboot", null));
                        td.Principal.RunLevel = TaskRunLevel.Highest;
                        ts.RootFolder.RegisterTaskDefinition("RemoteWindowsUpdater", td, TaskCreation.CreateOrUpdate, "SYSTEM", null, TaskLogonType.ServiceAccount);
                    }
                    Console.WriteLine("Scheduled task created successfully. Patches will occur at " + startTime.ToString() + ".");
                }
                catch (Exception e)
                {
                    LogMessage("RemoteWindowsUpdater", "Unable to register scheduled task. Error: " + e.ToString());
                    Console.WriteLine(e.ToString());
                }
            }
            else
            {
                LogMessage("RemoteWindowsUpdater", "Error in scheduling task. inMinutes set to 0");
                Console.WriteLine("Error in scheduling task. inMinutes set to 0");
            }
        }

        public static void Update(bool download, bool install, bool reboot)
        {
            if (download) DownloadUpdates();
            if (install) InstallUpdates();
            if (reboot) Reboot();
        }

        public static void SearchForUpdates(bool isOnline)
        {
            try
            {
                uSearcher = uSession.CreateUpdateSearcher();
                uSearcher.Online = isOnline;

                LogMessage("RemoteWindowsUpdater", "Searching for updates...");

                sResult = uSearcher.Search("IsInstalled=0");

                if (sResult.Updates.Count > 0)
                {
                    LogMessage("RemoteWindowsUpdater", "Found " + sResult.Updates.Count + " updates");
                    string strUpdates = "The following updates have been found\n=====\n";
                    foreach (IUpdate update in sResult.Updates)
                    {
                        //update.AcceptEula();
                        updatesToInstall.Add(update);
                        strUpdates += update.Title + "\n";
                    }

                    LogMessage("RemoteWindowsUpdater", strUpdates);
                }
            }
            catch (Exception exc)
            {
                LogMessage("RemoteWindowsUpdater", "Search Exception: " + exc.ToString(), true);
            }
        }

        public static void DownloadUpdates()
        {
            try
            {
                //Search for available updates
                SearchForUpdates(false);

                //Download available updates
                var downloader = uSession.CreateUpdateDownloader();
                downloader.Updates = updatesToInstall;

                LogMessage("RemoteWindowsUpdater", "Starting downloader...");
                downloader.Download();

                LogMessage("RemoteWindowsUpdater", "Downloader done!");
            }
            catch (Exception exc)
            {
                LogMessage("RemoteWindowsUpdater", "Download Exception: " + exc.ToString(), true);
            }
        }

        private static void InstallUpdates()
        {
            try
            {
                if (sResult == null) SearchForUpdates(false);

                if (sResult.Updates.Count > 0)
                {
                    var installer = uSession.CreateUpdateInstaller();
                    installer.Updates = updatesToInstall;

                    LogMessage("RemoteWindowsUpdater", "Starting installation...");

                    IInstallationResult installerResult = installer.Install();

                    string strFailedUpdates = "";
                    string strSuccessefulUpdates = "";

                    for (int i = 0; i < updatesToInstall.Count; i++)
                    {
                        string kbNumbers = "";
                        foreach (string n in updatesToInstall[i].KBArticleIDs) kbNumbers += n + ",";
                        kbNumbers = kbNumbers.Remove(kbNumbers.Length - 1);

                        if (installerResult.GetUpdateResult(i).HResult == 0)
                        {
                            strSuccessefulUpdates += updatesToInstall[i].Title + " installed successfully\n";
                        }
                        else
                        {
                            strFailedUpdates += updatesToInstall[i].Title + " -- Failed: Error code " + installerResult.GetUpdateResult(i).HResult + "\n";
                        }

                    }

                    string strLogMessage = strFailedUpdates + "\n" + strSuccessefulUpdates;
                    LogMessage("RemoteWindowsUpdater", "Installation done!");
                    LogMessage("RemoteWindowsUpdater", strLogMessage);
                }
                else
                {
                    LogMessage("RemoteWindowsUpdater", "No updates found. Exiting...");
                };
            }
            catch (Exception exc)
            {
                LogMessage("RemoteWindowsUpdater", "Installation Exception: " + exc.ToString(), true);
            }
        }

        public static void LogMessage(string source, string message, bool isException = false)
        {
            if (isException)
            {
                EventLog.WriteEntry(source, message, EventLogEntryType.Error);
                Console.WriteLine("Exception occurred...\n" + message);
            }
            else
            {
                EventLog.WriteEntry(source, message, EventLogEntryType.Information);
                Console.WriteLine(message);
            }
        }

        public static void Reboot()
        {
            System.Diagnostics.Process.Start("shutdown.exe", "-r -f -t 0");
        }
    }
}
